#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

# Silencing annoying warnings. Use options(warn=0) to activate

options(warn = -1)

# The required package are checked for and eventually installed (https://stackoverflow.com/a/4090208)

list.of.packages <- c("WikidataQueryServiceR", "rotl", "plyr", "dplyr", "tidyr", "readr")
new.packages <-
  list.of.packages[!(list.of.packages %in% installed.packages()[, "Package"])]
if (length(new.packages)) {
  install.packages(new.packages)
}

# test if there is at least one argument: if not, return an error
if (length(args) == 0) {
  stop("At least one argument must be supplied (input file).n", call. = FALSE)
} else if (length(args) == 1) {
  # default output file
  args[1] <- "wd2lotus_out.tsv"
}

# The required packages are loaded
suppressMessages(library(WikidataQueryServiceR))
suppressMessages(library(rotl))
suppressMessages(library(plyr))
suppressMessages(library(dplyr))
suppressMessages(library(tidyr))
suppressMessages(library(readr))


# Here we will used the query_wikidata function of the WikidataQueryServiceR to retrieve results according to a defined SPARQL query.
# This SPARQL query can be run directly here https://w.wiki/ooL it's objective is to retrieve entries with the "found in taxon" property https://www.wikidata.org/wiki/Property:P703

print("Launching the SPARQL query")

wd_mirror <-
  query_wikidata(
    'SELECT DISTINCT ?structure ?structure_inchikey ?structure_inchi ?structure_smiles_isomeric ?structure_smiles_canonical ?structure_cas ?structure_chebi ?structure_chembl ?structure_pubchem ?taxon ?taxon_name ?taxon_id_gbif ?taxon_id_ncbi ?reference ?reference_doi ?reference_title WHERE {
  VALUES ?classes {
    wd:Q11173
    wd:Q59199015
  }
  ?structure wdt:P235 ?structure_inchikey.
  OPTIONAL { ?structure wdt:P231 ?structure_cas. }
  OPTIONAL { ?structure wdt:P233 ?structure_smiles_canonical. }
  OPTIONAL { ?structure wdt:P234 ?structure_inchi. }
  OPTIONAL { ?structure wdt:P592 ?structure_chembl. }
  OPTIONAL { ?structure wdt:P662 ?structure_pubchem. }
  OPTIONAL { ?structure wdt:P683 ?structure_chebi. }
  OPTIONAL { ?structure wdt:P2017 ?structure_smiles_isomeric. }
  {
    ?structure p:P703 ?statement.
    ?statement ps:P703 ?taxon.
    OPTIONAL { ?taxon wdt:P225 ?taxon_name. }
    OPTIONAL { ?taxon wdt:P846 ?taxon_id_gbif. }
    OPTIONAL { ?taxon wdt:P685 ?taxon_id_ncbi. }
    {
      ?statement prov:wasDerivedFrom ?ref.
      ?ref pr:P248 ?reference.
      ?reference wdt:P356 ?reference_doi;
        wdt:P1476 ?reference_title.
    }
  }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}'
  )

# Once the query is over the organisms are deduplicated and NA are removed
if (is.na(args[2]) == FALSE) {
  print(paste("Treating the first", args[2], "of the SPARQL query"))
  wd_mirror <- wd_mirror %>%
    sample_n(args[2])
} else {
  print("Treating the full SPARQL query")
}


wd_mirror_unique_taxon <- wd_mirror %>%
  distinct(taxon_name) %>%
  drop_na(taxon_name)

# Them the rotl function are used to  1) Match taxonomic names to the Open Tree Taxonomy and 2) Extract the lineage information (higher taxonomy) from an object (ott_id) returned by the previous step

print("Matching the taxon names to Open Tree of Life")

taxa_query_wd <- tnrs_match_names(wd_mirror_unique_taxon$taxon_name)

print("Retrieving the lineage for each ott_id")

ft_wd <-
  tax_lineage(taxonomy_taxon_info(ott_id(taxa_query_wd), include_lineage = TRUE))

# We then add a link to each taxa for a display in otol website

taxa_query_wd$link <-
  paste(
    "https://tree.opentreeoflife.org/opentree/argus/ottol@",
    taxa_query_wd$ott_id,
    sep = ""
  )

# We match now match the taxonomical information this with the wikidata query output

# first lowercase the taxon_name column
wd_mirror <- wd_mirror %>%
  mutate(taxon_name = tolower(taxon_name))

# and left join both df

wd_mirror_otoled <-
  left_join(wd_mirror, taxa_query_wd, by = c("taxon_name" = "search_string"))

# here we would like to merge the previous df with a list of df

# first we can brutally merge the list of dfs

ft_wd_cat <- ldply(ft_wd, rbind)

# and then reshape then from long to with.default(

ft_wd_cat_wide <-
  reshape(ft_wd_cat,
    idvar = ".id",
    timevar = "rank",
    direction = "wide"
  )

# and we merge it back with the wd dframe

ft_wd_cat_wide$.id <- as.integer(ft_wd_cat_wide$.id)

wd_mirror_otoled_taxed <-
  left_join(wd_mirror_otoled, ft_wd_cat_wide, by = c("ott_id" = ".id"))

# exporting this as tsv

print(paste("Exporting results as tab delimited file named", args[1]))

write_tsv(wd_mirror_otoled_taxed, file = args[1])
