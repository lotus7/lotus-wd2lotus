# wd2lotus

Scripts allowing to fetch entries from Wikidata and annotate them with taxonomical information of the Open Tree of Life
in order to feed them in the LOTUS database.

## required packages

They should be directly installed if you don't have them present when your run the script for the first time. If you
have any difficulties or want more information on the packages please refer to :

- WikidataQueryServiceR package to fetch Wikidata entries from R https://github.com/wikimedia/WikidataQueryServiceR
- ROTL packages to match organisms with the Open Tree Of Life taxonomy https://github.com/ropensci/rotl

## SPARQL query

This (https://w.wiki/ooL) is the SPARQL query used to fetch entries having the "found in taxon"
property https://www.wikidata.org/wiki/Property:P703. It can be adapted directly in the wd2lotus.R script

## Running the script

### From a SPARQL query

Launch the wd2lotus_sqlited.R in your terminal. The script takes in two arguments, first the output file name and second
the numbers of line you want to treat (for testing purpose). Let the second argument empty to treat the full outcome of
the SPARQL query.

Example:

`Rscript --vanilla src/R/wd2lotus_sqlited.R data/out/test.tsv 50`

`Rscript --vanilla src/R/wd2lotus_sqlited.R data/out/wd_201217_sqlited.tsv`

### From a local file

Launch the local2lotus_sqlited.R in your terminal. The script takes in four arguments, first the input file name, second
the output filename, third the column name of the taxon to resolve, and fourth the numbers of line you want to treat (
for testing purpose). Let the fourth argument empty to treat the full outcome of the query.

Example:

`Rscript --vanilla local2lotus_sqlited.R ~/mondernierplatinumtoutbrillant.tsv local_sqlited.tsv organismCleaned`

## TODO

- [ ] add scripts to output a full OTOL taxonomy from a list of taxon names
- [ ] add scripts to output sub OTOL taxonomy per chemical compounds
- [ ] add cache option for wd ~~and rotl~~ queries
- [ ] work on qiime integration


